from MastoAPI import *
import fitz # pip install pymupdf frontend (Do NOT install fitz if using pymupdf)
from fitz.utils import getColor
from math import ceil
import requests
import html
import random
import os
from io import BytesIO
from PIL import Image

class PDFGen():
    @staticmethod
    def __strip_tags(s):
        tag = False
        quote = False
        out = ""

        for c in s:
            if c == '<' and not quote:
                tag = True
            elif c == '>' and not quote:
                tag = False
            elif (c == '"' or c == "'") and tag:
                quote = not quote
            elif not tag:
                out = out + c

        return out
 
    def __new_page(self):
        self.pagina = self.doc.new_page(pno=-1,width=1240,height=1754)
        self.pagina.insert_font(fontname="Roboto-Light", fontfile="fonts/roboto/Roboto-Light.ttf")
        self.pagina.insert_font(fontname="Roboto-Bold", fontfile="fonts/roboto/Roboto-Bold.ttf")
        self.pagina.insert_font(fontname="Icons", fontfile="fonts/software_kit_7.ttf")
        #pagina.insert_font(fontname="Roboto-Italic", fontfile="fonts/roboto/Roboto-Italic.ttf")
        #pagina.insert_font(fontname="Roboto-BoldItalic", fontfile="fonts/roboto/Roboto-BoldItalic.ttf")
        return self.pagina

    def __init__(self, thread_list):
        self.doc = fitz.open()
        self.pagina = self.__new_page()
        self.rLight = fitz.Font(fontname="Roboto-Light", fontfile="fonts/roboto/Roboto-Light.ttf")
        self.rBold = fitz.Font(fontname="Roboto-Bold", fontfile="fonts/roboto/Roboto-Bold.ttf", is_bold=True)
        self.thread_list = thread_list
        self.id = ceil(random.random() * 1000) + 1

    def update(self):
        title_url = self.thread_list[0]["url"]

        ly = self.insert_title("Pleroma Thread (generated with Maira Bot)", title_url) + 10

        for post in self.thread_list:
            r = requests.get(post["account"]["avatar"])
            ly = self.insert_post(post["content"], r.content, f"{post['account']['display_name']} ({post['account']['acct']}) - {post['created_at']}",
                ly, post["favourites_count"], post["reblogs_count"])
            if ly is None:
                self.pagina = self.__new_page()
                ly = self.insert_post(post["content"], r.content, f"{post['account']['display_name']} ({post['account']['acct']}) - {post['created_at']}",
                    50, post["favourites_count"], post["reblogs_count"])
            if len(post["media_attachments"]) > 0:
                lx = 0
                for media in post["media_attachments"]:
                    if not media["preview_url"].lower().endswith(('.png','.jpg','.jpeg','.gif', '.webp')):
                        continue
                    if ly + 256 >= 1604:
                        self.pagina = self.__new_page()
                        ly = 50
                    # File format check here
                    if media["preview_url"].lower().endswith(('.png','.jpg','.jpeg','.gif')):
                        r = requests.get(media["preview_url"])
                        self.insert_image_stream(r.content, 100 + lx, ly, 100 + lx + 256, ly + 256)
                        lx += 256
                    elif media["preview_url"].lower().endswith('.webp'):
                        im = Image.open(requests.get(media["preview_url"], stream=True).raw).convert("RGB")
                        bytbuf = BytesIO()
                        im.save(bytbuf, format='PNG')
                        im.close()
                        self.insert_image_stream(bytbuf, 100 + lx, ly, 100 + lx + 256, ly + 256)
                        lx += 256
                ly += 256

        self.docpath = f"./cache/threaddoc_{self.id}.pdf"
        self.doc.save(self.docpath, pretty=True, garbage=4, deflate=True)
        self.doc.close()
        return self.docpath

    def insert_image(self, path, x, y, sx, sy):
        with open(path, "rb") as f:
            self.pagina.insert_image((x,y,sx,sy), stream=f.read(), keep_proportion=True)

    def insert_image_stream(self, stream, x, y, sx, sy):
        self.pagina.insert_image((x,y,sx,sy), stream=stream, keep_proportion=True)

    def insert_post(self, text, avatar, name, start_y = 50, likes=0, reblogs=0):
        tw = fitz.TextWriter(self.pagina.rect, color=getColor("white"))
        tw.fill_textbox((100 + 100,start_y + 40, 100 + 900, 1754), html.unescape(PDFGen.__strip_tags(text)), font=self.rLight, fontsize = 14, align=fitz.TEXT_ALIGN_JUSTIFY)
        end_text_y = ceil(tw.last_point.y)
        
        if end_text_y > 1604:
            return None

        # Post container
        self.pagina.draw_rect((100, start_y, 100 + 1000, end_text_y + 50), fill=(0.18,0.26,0.36), color=1, stroke_opacity=0, fill_opacity=1.00, overlay=False)
        # User name
        self.pagina.insert_text(fitz.Point(100 + 100,start_y + 30), name, color=1, fontsize=16, fontname="Roboto-Bold")
        # Post text
        tw.write_text(self.pagina, color=getColor("white"))
        #Insert image
        self.insert_image_stream(avatar, 100 + 20, start_y + 15, 100 + 20 + 64, start_y + 15 + 64)
        # Likes / Reblogs
        self.pagina.insert_text(fitz.Point(100 + 100,end_text_y + 30), "*", color=getColor("gold"), fontsize=20, fontname="Icons")
        self.pagina.insert_text(fitz.Point(100 + 100 + 18,end_text_y + 30), str(likes), color=1, fontsize=16, fontname="Roboto-Light")
        self.pagina.insert_text(fitz.Point(100 + 100 + 100,end_text_y + 30), "W", color=getColor("gold"), fontsize=20, fontname="Icons")
        self.pagina.insert_text(fitz.Point(100 + 100 + 100 + 18,end_text_y + 30), str(reblogs), color=1, fontsize=16, fontname="Roboto-Light")

        self.doc.write()
        return end_text_y + 50

    def insert_title(self, title, subtitle):
        tw = fitz.TextWriter(self.pagina.rect, color=getColor("black"))
        tw.fill_textbox((100,50, 1100, 1754), title, font=self.rBold, fontsize = 28, align=fitz.TEXT_ALIGN_JUSTIFY)
        tw.write_text(self.pagina, color=getColor("black"))
        tw.fill_textbox((100,tw.last_point.y + 8, 1100, 1754), subtitle, font=self.rLight, fontsize = 18, align=fitz.TEXT_ALIGN_JUSTIFY)
        tw.write_text(self.pagina, color=getColor("black"))
        return tw.last_point.y

    def cleanup(self):
        os.remove(self.docpath)
