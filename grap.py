import matplotlib.pyplot as plt # pip install matplotlib
from matplotlib import cm, figure
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as mtick
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np # numpy
import math
from scipy import interpolate
from sympy import *

valid_types = ["line", "barv", "barh", "pie"]

class MairaGraphData:
    def __init__(self, set_x: list, set_y: list = None, set_z: list = None, label_x: str = None,
                label_y: str = None, label_z: str = None, type_: str = "line", title: str = None):
        if set_y is None:
            set_y = []
            for i in range(0,len(set_x)):
                set_y.append(i * 0.1)
        self.set_x = set_x
        self.set_y = set_y
        self.set_z = set_z
        self.label_x = label_x
        self.label_y = label_y
        self.label_z = label_z
        self.title = title
        if type_ in valid_types:
            self.graph_type = type_
        else:
            raise InvalidTypeException
    def graph(self) -> figure.Figure:
        mg = MairaGraph()
        return mg.graph(data=self)


class MairaGraph:
    def __init__(self):
        pass
    def gen_subplot(self, graphData: MairaGraphData, fig: figure.Figure, subp_row: int = 1, subp_col: int = 1, subp_id: int = 1):
        noaxes = False
        ax1 = fig.add_subplot(subp_row,subp_col,subp_id)
        if graphData.graph_type == "line":
            ax1.plot(graphData.set_x, graphData.set_y)
        elif graphData.graph_type == "barv":
            ax1.bar(graphData.set_x, graphData.set_y)
        elif graphData.graph_type == "barh":
            ax1.barh(graphData.set_x, graphData.set_y)
        elif graphData.graph_type == "pie":
            ax1.pie(graphData.set_y, labels = graphData.set_x, wedgeprops={'linewidth':1,'edgecolor':'white'},
                                    startangle=90, autopct='%1.1f%%')
            noaxes = True
        if graphData.title is not None:
            ax1.set_title(graphData.title)
        if not noaxes:
            ax1.set_ylabel(graphData.label_y)
            ax1.set_xlabel(graphData.label_x)
            ax1.grid(True)
            #ax1.set(xticks=np.arange(1,10),yticks=np.arange(1,10))
        return ax1
    def graph(self, data: MairaGraphData or list) -> figure.Figure:
        fig = None
        plt.clf()
        if type(data) is list:
            fig = plt.figure(num=0,figsize=(8,5))
            subp_graph = 1
            subp_graphs = len(data)
            if subp_graphs > 4:
                subp_col = 4
                subp_rows = len(data)/4
            else:
                subp_col = len(data)
                subp_rows = 1
            for graphData in data:
                subp = self.gen_subplot(graphData=graphData, fig=fig, subp_row=subp_rows, subp_col=subp_col, subp_id=subp_graph)
                subp_graph += 1
        elif type(data) is MairaGraphData:
            fig = plt.figure(num=0,figsize=(8,5))
            subp = self.gen_subplot(graphData=data, fig=fig)
        else:
            raise InvalidGraphDataException(f"Invalid graph data type ({type(data)})")
        return fig
    def plot(self, func: str, mathematica: bool = False) -> figure.Figure:
        var('x,y,z,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z')
        funcplt = None
        if mathematica:
            funcplt = parse_mathematica(func)
        else:
            funcplt = parse_expr(func, evaluate = False)
        graph = plot(funcplt, show=False, line_color="#3480eb", title=func)
        backend = graph.backend(graph)
        backend.process_series()
        return backend.fig

class InvalidTypeException:
    pass
class InvalidGraphDataException:
    pass