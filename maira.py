import os
import json
from MastoAPI import *
import re
import shlex # Split preserving spaces on quotes
from Debug import Debug
from MairaCommands import MairaCommands
import html

#valid_cmds = ["/graph", "/plot", "/rtd", "/stats", "/safebooru", "/ping", "/ocr", "/toNitter", "/searx", "/threadtopdf"]
#valid_opts = ["-x", "--x_data", "-y", "--y_data", "-lx", "--label-x", "-ly", "--label-y", "-t", "--type", "-tt", "--title",
#                "-f", "--function", "-wa", "--wolfram"]

config_data = {}
BOT_URL = "https://fedi.absturztau.be/Maira_Nekomori"
TAG_RE = re.compile(r'<[^>]+>')
TAG_MENTION = re.compile(r'(@[^\s]+)')
TAG_EMOJI = re.compile(r'(:[^\s]+:)')
TAG_TWITTER = re.compile(r'[http|https]+:\/\/twitter.com(.\S+)')
m = None
mairaCommands = None

Debug.set_config({
    "DEBUG_FILE": "./debug/maira.log",
    "ROLLER_LINES": 25000,
    "WRITE_TO_STDOUT": True
})

#class mairaListener(StreamListener):
class mairaListener(PleromaStreamListener):
    def on_update(self, status):
        pass
    def on_notification(self, not_):
        global m, pastebin, mairaCommands
        try:
            if not_ is None:
                Debug.debugLog("WARNING: NoneType notification on on_notification.", "NoneType notification")
                Debug.inspectLocals(locals(), ['not_'])
                return
            elif "status" not in not_:
                Debug.debugLog("WARNING: notification without an status on on_notification.", "No status on notification")
                Debug.inspectLocals(locals(), ['not_'])
                return
            notification = MastodonNotification(not_)
            status_ = notification.status
            if notification.type == "mention":
                if status_ is None:
                    Debug.debugLog("WARNING: No status on Mention", "No status on mention notification")
                    Debug.inspectLocals(locals(), ['not_', 'status_', 'notification'])
                    return
                content = status_.content
                for mention in status_.mentions:
                    if mention.acct == 'Maira_Nekomori':
                        text = html.unescape(remove_tags(content))
                        #com, argv, inv = cmdParser(text)
                        command_list = cmdParser(text)
                        if command_list == None:
                            # When no commands are issues
                            mairaCommands.postreplymsg("What should I do with that?", status_, status_.visibility, in_reply_to_id=status_.id)
                        elif len(command_list) < 1:
                            # Something went wrong :P
                            lid = Debug.debugLog("command_list is empty", "command_list is empty")
                            Debug.inspectLocals(locals(), ['command_list', 'text', 'content', 'status_'])
                            mairaCommands.postreplymsg(f"Call @enigmatico@fedi.absturztau.be and give him this: ({lid})", status_, status_.visibility)
                        else:
                            first = True
                            ret = None
                            for command in command_list:
                                if f"command_{command[0]}" in dir(mairaCommands):
                                    to_call = getattr(mairaCommands, f"command_{command[0]}")
                                    if first:
                                        ret = to_call(command[1], status_, True if len(command_list) < 2 else False)
                                        first = False
                                    else:
                                        ret = to_call(command[1], status_, False, ret)
                            if ret is not None:
                                mairaCommands.postreplymsg(f"{ret}", status_, status_.visibility, in_reply_to_id=status_.id)

        except Exception as exc:
            erid = Debug.debugException(exc, False, None, True, ['argv', 'com', 'notification', 'status_', 'mention', 'text']) 
            print(f"Error! {erid} ({str(exc)})")
            mairaCommands.post(f'@enigmatico@fedi.absturztau.be @{status_.account.acct} General error. Please debug. \n({erid})', visibility="unlisted")
    def on_abort(self, err):
        raise ConnectionAbortedException(f"Connection aborted: {str(err)}")

def remove_tags(text: str) -> str:
    nohtml = TAG_RE.sub('', text)
    nomention = TAG_MENTION.sub('', nohtml)
    return TAG_EMOJI.sub('', nomention).strip()

def cmdParser(argl: str) -> tuple:
    if '/' not in argl:
        return None
    unproc_commands = argl[argl.find('/')+1:].split('/')
    proc_commands = []
    for command in unproc_commands:
        argd: dict[str,str] = {}
        try:
            argv = shlex.split(command, posix=True)
        except ValueError as exc:
            print("ValueError")
            argv = shlex.split(command.replace("'", "\'").replace('"', '\"'), posix=True)

        if len(argv) > 0:
            cmd = argv[0]
        else:
            if len(unproc_commands) > 0:
                Debug.debugLog("WARNING: len(argv) was 0. Using unproc_commands", "cmdParser")
                cmd = unproc_commands[0]


        opts = [opt for opt in argv[1:] if opt.startswith("-") or opt.startswith("--")]
        args = [arg for arg in argv[1:] if not arg.startswith("-") and not arg.startswith("--") and not arg.startswith("/")]
        la = 0

        for opt in opts:

            argd[opt] = args[la]
            la += 1

        if len(argd) < 1:
            argd = argv[1:]
        proc_commands.append((cmd, argd))
    return proc_commands

def init() -> MastoAPI:
    global pastebin
    try:
        if os.path.isfile("config.json"):
            with open("config.json", "r") as f:
                d = f.read()
                config_data = json.loads(d)
            return MastoAPI(config_data["username"], config_data["password"], 'MairaBot',
                            config_data["instance"],'maira_clientcred.secret','maira_usercred.secret')
        else:
            raise ConfigFileException("Could not access the configuration file (config.json)")
    except Exception as exc:
        erid = Debug.debugException(exc, False, None, True, ['d', 'config_data', 'pastebin'])
        print(f"ERROR! {str(exc)} {erid}")

class ConfigFileException:
    pass
class ConnectionAbortedException:
    pass

if __name__ == "__main__":
    stream = None
    try:
        m = init()
        listener = mairaListener()
        mairaCommands = MairaCommands(m)
        stream = m.pleroma_stream_notifications(listener)
        while stream.running:
            time.sleep(1)
    except KeyboardInterrupt as ki:
        print("Keyboard interruption detected, closing...")
        if stream is not None:
            stream.close()
    except Exception as exc:
        Debug.debugException(exc, False, None, True, ['m', 'listener', 'stream', 'config_data'])