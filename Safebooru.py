from TorConnection import TorConnection
import requests
import json
import time
from io import BytesIO

HEADERS = {"Accept": "application/json",
    "Accept-Language": "en-US,en;q=0.5",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-origin",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"}

class SafebooruSearchResult:
    def __init__(self, hit: dict):
        self.directory = hit["directory"]
        self.hash = hit["hash"]
        self.height = hit["height"]
        self.id = hit["id"]
        self.image = hit["image"]
        self.change = hit["change"]
        self.owner = hit["owner"]
        self.parent_id = hit["parent_id"]
        self.rating = hit["rating"]
        self.sample = hit["sample"]
        self.sample_height = hit["sample_height"]
        self.sample_width = hit["sample_width"]
        self.score = hit["score"]
        self.tags = hit["tags"].split(' ')
        self.width = hit["width"]

class Safebooru:
    API_BASE = "https://safebooru.org/index.php?page=dapi&q=index&s=post&json=1"
    IMAGE_BASE = "https://safebooru.org//images"
    use_tor = True
    def setUseTor(self, use_tor: bool):
        self.use_tor = use_tor
    def search(self, tags: str or list) -> list:
        parsedtags = ""
        if isinstance(tags, list):
            parsedtags = '+'.join(tags)
        elif isinstance(tags,str):
            parsedtags = tags.replace(' ', '+')
        con = TorConnection()
        con.renew_connection()
        if self.use_tor:
            r = con.request_get_torified(f"{self.API_BASE}&tags={parsedtags}", headers=HEADERS)
        else:
            r = requests.get(f"{self.API_BASE}&tags={parsedtags}", headers=HEADERS)
        if len(r.text) < 1:
            return []
        if self.use_tor is True:
            # FUCK CLOUDFLARE. LIKE, SERIOUSLY, FUCK THEM BEYOND INFINITY
            # When that happens, renew the connection until you get an exit node that is not blocked
            while r.text[0] == '<':
                con.renew_connection()
                time.sleep(1)
                r = con.request_get_torified(f"{self.API_BASE}&tags={parsedtags}", headers=HEADERS)
            # I hope someone run them out of business. This would become a better world.
        if r.status_code == 200:
            data = json.loads(r.text)
            search_results = []
            for hit in data:
                search_results.append(SafebooruSearchResult(hit))
            return search_results
        else:
            return []
    def downloadImage(self, hit: SafebooruSearchResult, destination: str = None) -> bool:
        if self.use_tor:
            con = TorConnection()
            con.renew_connection()
            r = con.request_get_torified(f"{self.IMAGE_BASE}/{hit.directory}/{hit.image}", headers=HEADERS)
            if self.use_tor is True:
            # FUCK CLOUDFLARE. LIKE, SERIOUSLY, FUCK THEM BEYOND INFINITY
            # When that happens, renew the connection until you get an exit node that is not blocked
                while r.text[0] == '<':
                    con.renew_connection()
                    time.sleep(1)
                    r = con.request_get_torified(f"{self.IMAGE_BASE}/{hit.directory}/{hit.image}", headers=HEADERS)
            if r.status_code == 200:
                with open(destination if destination is not None else hit.image, "wb") as f:
                    f.write(r.content)
            else:
                return False
        else:
            print("Not torified, downloading raw")
            r = requests.get(f"{self.IMAGE_BASE}/{hit.directory}/{hit.image}", headers=HEADERS)
        if r.status_code == 200:
            with open(destination if destination is not None else hit.image, "wb") as f:
                f.write(r.content)
        else:
            return False
        return True