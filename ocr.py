import fitz
from pprint import pprint
import ocrmypdf
import io
import requests
import math
from PIL import Image, ImageOps, ImageEnhance

def ocr_imagen_de_internet(url: str, idioma: str = "eng+spa", contraste: float = 0.0) -> str:
    # Cabeceras HTTP
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0'}
    # Obtenemos la imágen usando requests como un flujo de datos.
    with requests.get(url, headers=headers, stream=True) as r:
        imgbytes = io.BytesIO()
        r.raise_for_status()
        for chunk in r.iter_content(chunk_size=8192):
            imgbytes.write(chunk)
        imgbytes.seek(0)
        # Cargamos la imágen con PIL desde el flujo de datos
        with Image.open(imgbytes) as im:
            # OCRMyPDF no trabaja con imágenes con transparencia, así que
            # comprobamos si hay transparencia y, si la hay, la eliminamos.
            if im.mode in ('RGBA', 'LA') or (im.mode == 'P' and 'transparency' in im.info):
                sinalpha = im.convert('RGB')
            else:
                sinalpha = im
            # Obtenemos los DPI del documento. Si no los especifica, le asignamos
            # 100 por defecto.
            imgdpi = 0
            if 'dpi' in im.info:
                imgdpi = im.info['dpi'][0]
            if imgdpi < 1:
                imgdpi = 100
            # Para intentar mejorar la calidad del escaneo, podemos convertir la
            # imágen a escala de gríses y ajustar el brillo usando PIL
            byn = ImageOps.grayscale(sinalpha)
            contrastador = ImageEnhance.Contrast(byn)
            contrastada = contrastador.enhance(contraste)
            # Creamos dos streams de bytes para entrada y salida de datos
            inbytes = io.BytesIO()
            outbytes = io.BytesIO()
            # Guardamos los datos de la imágen en el stream de entrada
            #contrastada.save(inbytes, format=im.format)
            sinalpha.save(inbytes, format=im.format)
            # No debemos olvidarnos de rebobinar el stream
            inbytes.seek(0)
            # Llamamos a ocrmypdf para que escanee la imágen y nos pase los datos como PDF
            ocrmypdf.ocr(
                inbytes,
                outbytes,
                image_dpi=int(math.floor(imgdpi)),
                language=idioma,
                progress_bar=False,
                output_type="pdf"
            )
            # Abrimos el PDF generado con fitz
            ocr_pdf = fitz.open("pdf", outbytes.getvalue())
            # Obtenemos el texto escaneado, ya como texto plano
            texto = ocr_pdf[0].get_text()
            # Cerramos el documento para liberar memoria
            ocr_pdf.close()
            # Devolvemos el texto
            return texto