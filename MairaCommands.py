import os
import json
from MastoAPI import *
import re
from grap import MairaGraphData, MairaGraph
from matplotlib import cm, figure
import io
import requests # Fuck you pastebin and fuck you everyone using cloudflare
import random
import numpy
import threading
import subprocess
from Safebooru import Safebooru
from PastebinAPI import Pastebin
from PIL import Image
from math import ceil, floor
from Debug import Debug
from ocr import ocr_imagen_de_internet
from Searx import Searx
from pdfgen import PDFGen
import html

BOT_URL = "https://fedi.absturztau.be/Maira_Nekomori"
TAG_RE = re.compile(r'<[^>]+>')
TAG_MENTION = re.compile(r'(@[^\s]+)')
TAG_EMOJI = re.compile(r'(:[^\s]+:)')
TAG_TWITTER = re.compile(r'[http|https]+:\/\/twitter.com(.\S+)')

def remove_tags(text: str) -> str:
    nohtml = TAG_RE.sub('', text)
    nomention = TAG_MENTION.sub('', nohtml)
    return TAG_EMOJI.sub('', nomention).strip()

def strToList(strlist: str) -> list:
    r = []
    strlist = strlist.replace('[','').replace(']','').replace(' ', '')
    strl = strlist.split(',')
    for str in strl:
        if isNumeric(str):
            r.append(float(str))
        else:
            r.append(str)
    return r

def isNumeric(val: str) -> bool:
    try:
        float(val)
        return True
    except ValueError:
        return False

class MairaCommands:
    def __init__(self, connection):
        self.m = connection
        self.mg = MairaGraph()
    def postmsg(self, msg: str, status: MastodonPost, visibility: str = "public", media_ids: list = None, in_reply_to_id = None):
        self.m.post(msg=f'{msg}', visibility=visibility, media_ids=media_ids, in_reply_to_id=in_reply_to_id)

    def postreplymsg(self, msg: str, status: MastodonPost, visibility: str = "public", media_ids: list = None, in_reply_to_id = None):
        mentions = f"@{status.account.acct} "
        for mention in status.mentions:
            mentions += f"@{mention.acct} "
        self.m.post(msg=f'{msg}\n{mentions}', visibility=visibility, media_ids=media_ids, in_reply_to_id=in_reply_to_id)

    def postfig(self, fig: figure.Figure, status: MastodonPost):
        d = io.BytesIO()
        fig.savefig(d,format="png")
        d.seek(0)
        r = self.m.upload(media_file=d)
        rid = [int(r.id)]
        self.m.post(status="@" + status.account.acct + " Here is your graph!", media_ids=rid,
                    visibility="unlisted", in_reply_to_id=status.id)

    def postimg(self, img: str):
        return self.m.upload(img)

    def post(self, text:str):
        self.m.quickpost(text)

    def graph_command(self, argv, status_, output = True, pipe_data = None):
        ydata = argv["-y"] if "-y" in argv else argv["--y-data"] if "--y-data" in argv else None
        xdata = argv["-x"] if "-x" in argv else argv["--x-data"] if "--x-data" in argv else None
        ylabel = argv["-ly"] if "-ly" in argv else argv["--label_y"] if "--label_y" in argv else None
        xlabel = argv["-lx"] if "-lx" in argv else argv["--label_x"] if "--label_x" in argv else None
        type_ = argv["-t"] if "-t" in argv else argv["--type"] if "--type" in argv else "line"
        title_ = argv["-tt"] if "-tt" in argv else argv["--title"] if "--title" in argv else None
        data = MairaGraphData(set_y=strToList(ydata), set_x=strToList(xdata), label_y = ylabel, label_x = xlabel,
            type_=type_, title=title_)
        fig = data.graph()
        self.postfig(fig,status_)

    def command_ping(self, argv, status_, output = True, pipe_data = None):
        try:
            self.postreplymsg(f'Pong', status_, status_.visibility, in_reply_to_id=status_.id)
        except Exception as exc:
            erid = Debug.debugException(exc, False, None, True)
            self.postreplymsg(f'Error while pinging\n({erid})', status_, status_.visibility, in_reply_to_id=status_.id)
    
    def command_threadtopdf(self, argv, status_, output = True, pipe_data = None):
        try:
            origin = status_.in_reply_to_id
            if origin is not None:
                thread_list = []
                post = self.m.get_status(status_.in_reply_to_id)
                if post is not None:
                    thread_list.insert(0, post)
                while post["in_reply_to_id"] is not None:
                    post = self.m.get_status(post["in_reply_to_id"])
                    if post is not None:
                        thread_list.insert(0, post)
                pdf = PDFGen(thread_list)
                pdfile = pdf.update()
                res = self.m.session.media_post(pdfile, mime_type="application/pdf")
                self.postreplymsg(f"This thread has been converted to a PDF. The PDF is attached to this post.", status_, status_.visibility, [res["id"]], in_reply_to_id=status_.id)
                pdf.cleanup()
        except Exception as exc:
            erid = Debug.debugException(exc, False, None, True)
            self.postreplymsg(f'Error: threadtopdf: \n({erid})', status_, status_.visibility, in_reply_to_id=status_.id)

    def command_toNitter(self, argv, status_, output = True, pipe_data = None):
        try:
            argv = ' '.join(argv)
            res = re.findall(TAG_TWITTER, argv)
            rstring = ""
            if len(res) < 1:
                self.postreplymsg(f'No twitter links found.', status_, status_.visibility, in_reply_to_id=status_.id)
            else:
                i = 0
                for link in res:
                    rstring += f'{i} https://nitter.absturztau.be{link}\n'
                self.postreplymsg(f'{rstring}', status_, status_.visibility, in_reply_to_id=status_.id)
        except Exception as exc:
            erid = Debug.debugException(exc, False, None, True)
            self.postreplymsg(f'Error: toNitter: \n({erid})', status_, status_.visibility, in_reply_to_id=status_.id)

    def command_searx(self, argv, status_, output = True, pipe_data = None):
        try:
            search = ' '.join(argv)
            results = Searx.search(search)
            if len(results) < 1:
                self.postreplymsg(f'No results.', status_, status_.visibility, in_reply_to_id=status_.id)
            else:
                resultmsg = ""
                for i in range(0, 10 if len(results) > 10 else len(results)):
                    resultmsg += f"{results[i][0]}: {results[i][3][0:40]}...\n{results[i][1]}\n\n"
                mentions = f"@{status_.account.acct} "
                for mention in status_.mentions:
                    mentions += f"@{mention.acct} "
                self.postreplymsg(f'{resultmsg}', status_, status_.visibility, in_reply_to_id=status_.id)
        except Exception as exc:
            erid = Debug.debugException(exc, False, None, True)
            self.postreplymsg(f'Error while searxing\n({erid})', status_, status_.visibility, in_reply_to_id=status_.id)

    def command_ocr(self, argv, status_, output = True, pipe_data = None):
        try:
            for media in status_.media_attachments:
                ocrtxt = ocr_imagen_de_internet(media.url, "eng")
                if len(ocrtxt) < 1:
                    self.postreplymsg(f'OCR scan failed (Empty)', status_, status_.visibility, in_reply_to_id=status_.id)
                #elif 0 < len(ocrtxt) <= 410:
                else:
                    self.postreplymsg(f'OCR result: {ocrtxt}', status_, status_.visibility, in_reply_to_id=status_.id)
                #else:
                #    id = f"{random.randrange(10000,999999)}{random.randrange(10000,999999)}"
                #    url = pastebin.paste(f"-- OCR SCANNED BY MAIRA (https://mastodon.social/web/@Maira_Nekomori) --\n\n{str(ocrtxt)}", f"Maira bot stats result id {id}")
                #    postreplymsg(f'@{status_.account.acct} OCR result: {url}', status_, status_.visibility, in_reply_to_id=status_.id)
        except Exception as exc:
            erid = Debug.debugException(exc, False, None, True, ['argv', 's', 'ocrtxt' ,'url', 'media'])
            self.postreplymsg(f'Error while calling ocr:\n({erid})', status_, status_.visibility, in_reply_to_id=status_.id)

    def command_safebooru(self, argv, status_, output = True, pipe_data = None):
        try:
            s = Safebooru()
            s.setUseTor(False)
            sr = s.search(argv)
            if not isinstance(sr, list):
                self.postreplymsg(f'Something went wrong with your search. Try again maybe.', status_, "unlisted", in_reply_to_id=status_.id)
                return
            if len(sr) < 1:
                self.postreplymsg(f'Your search produced no results. make sure you are using the right tags or try again with different tags.', status_, "unlisted")
                return
            hit = random.choice(sr)
            s.downloadImage(hit, f"./cache/{hit.image}")
            if hit.width >= 1024 or hit.height >= 1024:
                img = Image.open(f"./cache/{hit.image}")
                resized = img.resize((floor(img.size[0]/2),floor(img.size[1]/2)))
                resized.save(f"./cache/{hit.image}")
            
            imgid = self.postimg(f"./cache/{hit.image}")[0].id
            self.postreplymsg(f'I hope you like this image!', status_, status_.visibility, [imgid], in_reply_to_id=status_.id)
            os.remove(f"./cache/{hit.image}")
        except requests.exceptions.ConnectionError as exc:
            erid = Debug.debugException(exc, False, None, True, ['argv', 's', 'sr', 'hit', 'img', 'resized', 'imgid'])
            self.postreplymsg(f'There was a connection error while searching for your image. Please try again\n({erid})', status_, status_.visibility)

    def command_rtd(self, argv, status_, output = True, pipe_data = None):
        try:
            results = []
            maxthrows = 10
            throws = 0
            for dice in argv:
                dicedata = dice.split('d')
                diceresults = []
                for i in range(len(dicedata)):
                    dicedata[i] = int(dicedata[i])
                if dicedata[0] > 10:
                    dicedata[0] = 10
                if dicedata[1] > 100:
                    dicedata[1] = 100
                for i in range(0, dicedata[0]):
                    diceresults.append(random.randrange(1,dicedata[1]+1))
                throws += 1
                if throws == maxthrows:
                    break
                results.append({f"{dice}": diceresults, "sum": numpy.sum(diceresults), "mean": numpy.mean(diceresults), "median": numpy.median(diceresults)})
                self.postreplymsg(str(results),status_, status_.visibility, in_reply_to_id=status_.id)
        except Exception as exc:
            erid = Debug.debugException(exc, False, None, True, ['argv' 'com', 'text', 'results', 'maxthrows', 'throws', 'dicedata','diceresults'])
            self.postreplymsg(f'Error on "rtd"\n({erid})', status_, status_.visibility, in_reply_to_id=status_.id)

    def command_stats(self, argv, status_, output = True, pipe_data = None):
        try:
            results = []
            for data in argv:
                arrs = json.loads(data)
                if isinstance(arrs[0], list):
                    results.append({
                        "data": arrs,
                        "sum": [[numpy.sum(ar) for ar in arrs], numpy.sum(arrs)],
                        "prod": [[numpy.prod(ar) for ar in arrs], numpy.prod(arrs)],
                        "mean": [[numpy.mean(ar) for ar in arrs], numpy.mean(arrs)],
                        "median": [[numpy.median(ar) for ar in arrs], numpy.median(arrs)],
                        "std": [[numpy.std(ar) for ar in arrs], numpy.std(arrs)],
                        "ptp": [[numpy.ptp(ar) for ar in arrs], numpy.ptp(arrs)],
                        "percentiles": [[numpy.percentile(ar,[20,40,60,80]) for ar in arrs], numpy.percentile(arrs,[20,40,60,80])],
                        "var": [[numpy.var(ar) for ar in arrs], numpy.var(arrs)],
                        "pcc": numpy.corrcoef(arrs),
                        "fft2/ifft2": [numpy.fft.fft2(arrs), numpy.fft.ifft2(arrs)]
                    })
                elif isinstance(arrs[0], int) or isinstance(arrs[0], float):
                    results.append({
                        "data": arrs,
                        "sum": numpy.sum(arrs),
                        "prod": numpy.prod(arrs),
                        "mean": numpy.mean(arrs),
                        "median": numpy.median(arrs),
                        "std": numpy.std(arrs),
                        "ptp": numpy.ptp(arrs),
                        "percentiles": numpy.percentile(arrs,[20,40,60,80]),
                        "var": numpy.var(arrs),
                        "fft/ifft": [numpy.fft.fft(arrs), numpy.fft.ifft(arrs)]
                    })
            self.postreplymsg(str(results),status_, visbility=status_.visibility, in_reply_to_id=status_.id)
        except Exception as exc:
            erid = Debug.debugException(exc, False, None, True, ['argv', 'id', 'url', 'arrs', 'results', 'data'])
            self.postreplymsg(f'Error on "stats"\n({erid})', status_, status_.visibility, in_reply_to_id=status_.id)

    def command_plot(self, argv, status_, output = True, pipe_data = None):
        ft = argv["-f"] if "-f" in argv else argv["--function"]
        wa = argv["-wa"] if "-wa" in argv else argv["--wolfram"] if "--wolfram" in argv else False
        fig = self.mg.plot(ft, wa)
        self.postfig(fig,status_)

    def command_debug_test(self, argv, status_, output = True, pipe_data = None):
        self.postreplymsg(f"argv: {argv}", status_, status_.visibility, in_reply_to_id=status_.id)

    def command_raytrace(self, argv, status_, output = True, pipe_data = None):
        if status_.content.find('@@begin') < 0 or status_.content.find('@@end') < 0:
            if status_.content.find('@@begin') < 0:
                self.postreplymsg(f"Error: @@begin not found", status_, status_.visibility, in_reply_to_id=status_.id)
            elif status_.content.find('@@end') < 0:
                self.postreplymsg(f"Error: No matching @@end found", status_, status_.visibility, in_reply_to_id=status_.id)
            return
        script = html.unescape(remove_tags(status_.content[status_.content.find('@@begin')+7:status_.content.find('@@end')].strip().replace('<br/>','\n')))
        rval = random.randrange(9999,99999999)
        with open(f'./temp/{rval}.pov', 'w') as f:
            f.write(script)
        rcode = subprocess.call(["D:\\RETRO\\POV-Ray\\v3.7\\bin\\pvengine64.exe", "/NR", "/EXIT", "/RENDER", os.path.abspath(f'./temp/{rval}.pov')])
        if os.path.exists(f'./temp/{rval}.png'):
            mid = self.m.session.media_post(f'./temp/{rval}.png', mime_type="image/png")
            self.postreplymsg(f"Rendering completed!", status_, status_.visibility, in_reply_to_id=status_.id, media_ids=[mid["id"]])
            os.remove(f'./temp/{rval}.png')
        else:
            self.postreplymsg(f"No output (Process returned with retcode {rcode})", status_, status_.visibility, in_reply_to_id=status_.id)
        os.remove(f'./temp/{rval}.pov')

    def command_debug_sum(self, argv, status_, output = True, pipe_data = None):
        if pipe_data is not None:
            if not isinstance(pipe_data, list):
                pipe_data = [int(pipe_data)]
            else:
                for arg in range(0, len(pipe_data)):
                    pipe_data[arg] = int(pipe_data[arg])
            res = numpy.sum(pipe_data)
        else:
            for arg in range(0, len(argv)):
                argv[arg] = int(argv[arg])
            res = numpy.sum(argv)
        if output is True:
            self.postreplymsg(f"The sum of {', '.join((str(c) for c in argv) if pipe_data is None else (str(e) for e in pipe_data))} is {str(res)}", status_, status_.visibility, in_reply_to_id=status_.id)
        else:
            return res

    def command_debug_pipe_square(self, argv, status_, output = True, pipe_data = None):
        if len(argv) < 1 and output is True and pipe_data is None:
            self.postreplymsg("No data given", status_, status_.visibility, in_reply_to_id=status_.id)
            return None
        elif len(argv) < 1 and output is False and pipe_data is None:
            return None
        va = 0
        if pipe_data is None:
            va = int(argv[0])
        else:
            va = int(pipe_data)
        result = va ** 2
        if output is True:
            self.postreplymsg(f"The square of {va} is {str(result)}", status_, status_.visibility, in_reply_to_id=status_.id)
        else:
            return result