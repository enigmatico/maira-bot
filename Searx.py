import requests
import json

INSTANCE = "https://searx.absturztau.be"
ENDPOINT = "/search"

class Searx:
    @staticmethod
    def __formencode(searchstring: str, page: int = 1, category: int = 1, time_range: str = None, language: str = "en-US",
                    format: str = "json"):
        return {
            "category_general": str(category),
            "q": searchstring,
            "pageno": str(page),
            "time_range": time_range,
            "language": language,
            "format": format
        }

    @staticmethod
    def setinstance(instance_base_url: str):
        global INSTANCE
        INSTANCE = instance_base_url
        return INSTANCE

    @staticmethod
    def search(search_string: str, page: int = 1):
        result_list = []
        searchstr = Searx.__formencode(search_string, page)
        res = requests.post(f'{INSTANCE}{ENDPOINT}', data=searchstr)
        if res.status_code == 200:
            jsonresults = json.loads(res.content.decode("utf-8"))
            if len(jsonresults["results"]) < 1:
                return []
            for result in jsonresults["results"]:
                if "title" in result and "url" in result:
                    result_list.append((result["title"], result["url"], result["engine"] if "engine" in result else "", result["content"] if "content" in result else ""))
            return result_list